let tabs = document.querySelectorAll("[data-tab-target")
let content = document.querySelectorAll(".tab-pane")

// listen to each tab click
tabs.forEach(tab => {
  tab.addEventListener("click", () => {
    // get the content tab with the same tab label
    let targetContent = document.querySelector(tab.dataset.tabTarget)

    // close all content
    content.forEach(tabContent => {
      tabContent.classList.remove("active")
    })

    // but open the content that has the same tab label
    targetContent.classList.add("active")
  })
})