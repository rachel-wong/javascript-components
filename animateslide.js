let listitem = [...document.querySelectorAll(".animateitem")]

let options = {
  rootMargin: '10%', // this controls how quickly it runs when it intersects (negative value creates a delay lag)
  threshold: 0.0
}

let observer = new IntersectionObserver(showItem, options);

function showItem(entries) {
  entries.forEach(entry => {
    if (entry.isIntersecting == true) {
      // each entry contains an array of letters and span spaces inside the child
      console.log(entry.target)
      let letters = [...entry.target.querySelectorAll('span')]
      letters.forEach((letter, idx) => {
        setTimeout(() => {
          letter.classList.add("active")
        }, idx * 50)
      })
      entry.target.children[0].classList.add("active")
    } else {
      entry.target.children[0].classList.remove("active")
    }
  })
}

// key attribute to watch for is isIntersecting boolean.
listitem.forEach(item => {
  let newString = '';
  //converts each line into letters and span spaces
  let itemText = item.children[0].innerText.split("")
  itemText.map(letter => (
    newString += letter == " " ? `<span class="gap"></span>` : `<span>${letter}</span>`
  ))

  item.innerHTML = newString;
  observer.observe(item)
})
