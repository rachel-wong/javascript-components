let toggles = document.querySelectorAll(".toggle")
let contentDiv = document.querySelectorAll(".content")

// opens any one of them
// toggles.forEach(toggle => {
//   toggle.addEventListener("click", () => {
//     if (toggle.nextElementSibling.classList.contains('content')) {
//       let contentDiv = toggle.nextElementSibling
//       if (parseInt(contentDiv.style.height) != contentDiv.scrollHeight) {
//         contentDiv.style.height = contentDiv.scrollHeight + "px"
//       } else {
//         contentDiv.style.height = 0
//       }
//     }
//   })
// })

// opens only one of them (has error)
toggles.forEach((toggle, toggleIndex) => {
  toggle.addEventListener("click", () => {
    if (toggle.nextElementSibling.classList.contains('content')) {
      let contentDiv = toggle.nextElementSibling
      if (parseInt(contentDiv.style.height) != contentDiv.scrollHeight) {
        contentDiv.style.height = contentDiv.scrollHeight + "px"
      } else {
        contentDiv.style.height = 0
      }
    }

    contentDiv.forEach((content, contentIndex) => {
      if (contentIndex !== toggleIndex) {
        content.style.height = "0px"
      }
    })
  })
})

// opens only one of them (no error)
// for (let i = 0; i < toggles.length; i++) {
//   toggles[i].addEventListener("click", () => {

//     // is the current contentDiv being selected at its natural height? if not, make it natural scroll_height (open)
//     // otherwise close it
//     if (parseInt(contentDiv[i].style.height) != contentDiv[i].scrollHeight) {
//       contentDiv[i].style.height = contentDiv[i].scrollHeight + "px"
//     } else {
//       contentDiv[i].style.height = "0px"
//     }

//     // if contentDiv is not the current contentDiv being selected, then close it
//     for (let j = 0; j < contentDiv.length; j++) {
//       if (j !== i) {
//         contentDiv[j].style.height = "0px"
//       }
//     }
//   })
// }
