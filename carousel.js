const slider = document.querySelector(".slider") // this gets moved left and right

const leftArrow = document.querySelector(".left")
const rightArrow = document.querySelector(".right")

// this counts which slide you are up to and used to increment the amount of 25% distance to move
let sectionIndex = 0

let pagers = [...document.querySelectorAll(".pager")]

// based on the index number, translate the amount to move
pagers.forEach((pager, index) => {
  pager.addEventListener("click", () => {
    console.log(index)
    slider.style.transform = 'translate(' + (index) * -25 + '%)';
  })
})

rightArrow.addEventListener("click", () => {
  sectionIndex = sectionIndex < 3 ? sectionIndex + 1 : 3 // brittle
  console.log('sectionIndex:', sectionIndex)
  slider.style.transform = 'translate(' + (sectionIndex) * -25 + '%)';
})

leftArrow.addEventListener("click", () => {
  sectionIndex = sectionIndex > 0 ? sectionIndex - 1 : 0 // brittle
  console.log('sectionIndex:', sectionIndex)
  slider.style.transform = 'translate(' + (sectionIndex) * -25 + '%)';
})