window.addEventListener("scroll", () => {
  let revealboxes = document.querySelectorAll(".reveal")

  revealboxes.forEach(box => {
    let windowHeight = window.innerHeight;
    let revealTop = box.getBoundingClientRect().top;
    // when scroll to the top of the box, then reveal
    // otherwise do not apply the active class
    let revealpoint = 150;

    if (revealTop < windowHeight - revealpoint) {
      box.classList.add("active")
    } else {
      box.classList.remove("active")
    }
  })
})